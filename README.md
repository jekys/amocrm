### Инициализация объекта для работы с API ###

~~~~
$subdomain = 'Поддомен в AmoCrm';
$login = 'Логин от AmoCrm';
$hash = 'Хеш из профиля в AmoCrm';

$amoCrm = new AmoCrm\Api($subdomain, $login, $hash);
~~~~

### Получение списка пользователей ###

~~~~
$amoCrm = new AmoCrm\Api($subdomain, $login, $hash);
$account = $amoCrm->getAccount(['users']);

echo '<pre>';
var_dump($account['_embedded']['users']);
echo '</pre>';
~~~~

### Получение списка воронок и стадий сделок ###

~~~~
$amoCrm = new AmoCrm\Api($subdomain, $login, $hash);
$account = $amoCrm->getAccount(['piplines']);

echo '<pre>';
var_dump($account['_embedded']['piplines']);
echo '</pre>';
~~~~

### Получение списка элементов сущности ###

Возможные сущности:

* leads - сделки
* contacts - контакты
* companies - компании
* customers - покупатели
* tasks - задачи
* notes - события (примечания)
* pipelines - воронки и этапы продаж

У каждой сущности доступен свой набор для фильтрации и постраничной навигации, возможные значения для каждой сущности можно посмотреть [в докуметнации](https://www.amocrm.ru/developers/content/api/recommendations)

Пример получения списка сделок (без памаметров limit_rows и limit_offset достанет не более 500 элементов):

~~~~
$amoCrm = new AmoCrm\Api($subdomain, $login, $hash);

$entity = 'leads';
$params = [];
$entityList = $amoCrm->getList($entity, $params);

echo '<pre>';
var_dump($account['_embedded']['piplines']);
echo '</pre>';
~~~~

### Загрузка файла по имени ###

~~~~
$amoCrm = new AmoCrm\Api($subdomain, $login, $hash);
$fileName = 'QRzD_1111.png';
$savedFilePath = $amoCrm->saveAttachment('QRzD_1111.png');
echo $savedFilePath;
~~~~

### Получение информации о последнем запросе (curl_getinfo) ###

~~~~
$amoCrm = new AmoCrm\Api($subdomain, $login, $hash);
echo '<pre>';
var_dump($amoCrm->lastCallInfo());
echo '</pre>';
~~~~


