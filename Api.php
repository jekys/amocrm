<?php
namespace AmoCrm;

/**
 * Class for using AmoCrm APIv2
 * 
 * @author E.Prokhorov
 * @see https://www.amocrm.ru/developers/content/api/recommendations
*/
class Api 
{
	/**
	 * cURL handler
	 * 
	 * @var $_curl
	 */
	private $_curl;

	/**
	 * Base path of URL for API calls
	 * 
	 * @var $_apiUrl
	 */
	private $_apiUrl;

	/**
	 * Class constructor
	 * Prebuild cURL with some options and authorize user in AmoCrm API
	 * 
	 * @param string $subdomain
	 * @param string $login
	 * @param string $hash
	 * 
	 * @return void
	 */
	public function __construct($subdomain, $login, $hash)
	{
		$this->_apiUrl = 'https://'.$subdomain.'.amocrm.ru/';
		$this->_curl = curl_init();

		curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($this->_curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($this->_curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
		curl_setopt($this->_curl, CURLOPT_HEADER, false);
		curl_setopt($this->_curl, CURLOPT_COOKIEFILE, dirname(__FILE__).'/cookie_'.$subdomain.'.txt');
		curl_setopt($this->_curl, CURLOPT_COOKIEJAR, dirname(__FILE__).'/cookie_'.$subdomain.'.txt');
		curl_setopt($this->_curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($this->_curl, CURLOPT_SSL_VERIFYHOST, 0);

		$this->__auth($login, $hash);
	}

	/**
	 * Returns cUrl info from last request
	 * 
	 * @return array
	 */
	public function lastCallInfo()
	{
		return curl_getinfo($this->_curl);
	}

	/**
	 * Making calls to AmoCrm API 
	 * 
	 * @param string $path
	 * @param array $params
	 * @param string $type
	 * @param bool $jsonResponse
	 * 
	 * @return mixed
	 */
	private function __request($path, $params = [], $type = 'GET', $jsonResponse = true)
	{
		if ($type == 'POST') {
			curl_setopt($this->_curl, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($this->_curl, CURLOPT_POSTFIELDS, json_encode($params));
		} else {
			curl_setopt($this->_curl, CURLOPT_CUSTOMREQUEST, 'GET');
			curl_setopt($this->_curl, CURLOPT_POSTFIELDS, []);
			
			if (!empty($params)) {
				$path .= '?'.http_build_query($params);
			}
		}

		if ($jsonResponse) {
			curl_setopt($this->_curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		} else {
			curl_setopt($this->_curl, CURLOPT_HTTPHEADER, []);
		}

		curl_setopt($this->_curl, CURLOPT_URL, $this->_apiUrl.$path);

		$response = curl_exec($this->_curl);

		$code = curl_getinfo($this->_curl, CURLINFO_HTTP_CODE);

		if ($jsonResponse) {
			$response = json_decode($response, true);
		}

		return $response;
	}

	/**
	 * Authorizes the user on the system
	 * @see https://www.amocrm.ru/developers/content/api/auth
	 * 
	 * @param string $login
	 * @param string $hash
	 * 
	 * @return array
	 */
	private function __auth($login, $hash)
	{
		$params = [
			'USER_LOGIN' => $login,
			'USER_HASH' => $hash
		];

		return $this->__request('private/api/auth.php?type=json', $params, 'POST');
	}

	/**
	 * Returns info about AmoCrm account
	 * @see https://www.amocrm.ru/developers/content/api/account
	 * 
	 * @param array $with
	 * 
	 * @return array
	 */
	public function getAccount($with = [])
	{
		$params = [];
		if (!empty($with)) {
			$params = [
				'with' => implode(',', $with)
			];
		}

		return $this->__request('api/v2/account', $params);
	}

	/**
	 * Return list of entity items
	 * 
	 * @param string $entity,
	 * @param array $params
	 *
	 * @return array
	 */
	public function getList($entity, $params = [])
	{
		$possibleParams = [
			'limit_rows',
			'limit_offset',
			'id',
			'query',
			'responsible_user_id',
			'status',
			'filter',
			'element_id',
			'type',
			'note_type'
		];

		$requestParams = [];

		foreach ($possibleParams as $paramName) {
			if (!empty($params[$paramName])) {
				$requestParams[$paramName] = $params[$paramName];
			}
		}

		return $this->__request('api/v2/'.$entity, $requestParams);
	}

	/**
	 * Saving attachment file from AmoCrm '/download/' directory to the path on the server
	 * Return full path to saved file from root directory
	 * 
	 * @param string $fileName
	 * @param string $savePath
	 * 
	 * @return string
	 */ 
	public function saveAttachment($fileName, $savePath = 'upload')
	{
		$fileData = $this->__request('download/'.$fileName, [], 'GET', false);
		if ($fileData) {
			$filePath = $_SERVER['DOCUMENT_ROOT'].'/'.$savePath.'/'.$fileName;
			if (file_put_contents($filePath, $fileData) !== false) {
				return $filePath;
			}
		}

		return false;
	}

	/**
	 * Class desctructor
	 * Unset cURL handler
	 * 
	 * @return void
	 */
	public function __destruct()
	{
		curl_close($this->_curl);
	}
}